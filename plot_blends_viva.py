
# plot ALL 20 blend simulations 
# on ONE histogram! Just gives an 
# idea of the variation

# In[]:

import os, sys

from astropy.io         import ascii as ascii_file
from astropy.table      import Table
from matplotlib.ticker  import MultipleLocator, FormatStrFormatter

import seaborn      as sns
import numpy        as np
import matplotlib
import matplotlib.pyplot as plt


# my handy utilities
sys.path.append('../')
from common  import utils


# In[]:

blend_header=[]
blend_header.append('star_id')
blend_header.append('star_mass')
blend_header.append('star_luminosity')
blend_header.append('binary_id')
blend_header.append('binary_evol')
blend_header.append('binary_mass1')
blend_header.append('binary_mass2')
blend_header.append('star_mag_r')
blend_header.append('binary_mag_r')
blend_header.append('delta_mag')
blend_header.append('Period')
blend_header.append('a')
blend_header.append('transit_probability')
blend_header.append('binary_radius1')
blend_header.append('binary_radius2')
blend_header.append('binary_luminosity1')
blend_header.append('binary_luminosity2')
blend_header.append('actual_transit depth 1')
blend_header.append('actual_transit depth 2')
blend_header.append('measured_transit_depth_1')
blend_header.append('measured_transit_depth_2')

# In[]:

# base_folder = '/padata/beta/users/efarrell/data/plato'
base_folder = '/padata/beta/users/efarrell/data/old_platos/thurs_plato'

# data_folder = base_folder + '/blend_output/run_3' # imf kroupka
# data_folder = base_folder + '/blend_output/run_4' # new jktepbop sufrace brightness
# data_folder = base_folder + '/blend_output/run_5' # 20 simulations
data_folder = base_folder + '/blend_output/run_4' # 1x1 deg 


grid_square = '16'

# load files names with simulated data
# into a dictionary object
pattern     = os.path.join(data_folder, grid_square, "all_blends*")
sim_files   = utils.load_files(glob_pattern=pattern, headers=blend_header)




# In[]:

# where will we save the plots to?
plot_folder = 'plots'
utils.rotate_folder(plot_folder)


# In[]:

# histogram of transit depths
# for each simulated blend file

all_depth1 = np.array([], dtype=np.float64)
all_depth2 = np.array([], dtype=np.float64)

fig = plt.figure(figsize=(17, 10))
ax = plt.subplot(111)

for file_name, sim_data in sim_files.iteritems():


    # binsize=500
    binsize=1000
    # binsize=50
    # binsize=20 # for 2nd eclipse

    plot_filename = os.path.splitext(os.path.basename(file_name))[0]
    print "processing: " + plot_filename 


    depth1 = sim_data['measured_transit_depth_1'].data
    depth2 = sim_data['measured_transit_depth_2'].data

    mask1 = (depth1 > 0)
    mask2 = (depth2 > 0)

    # mask1 = (depth1 > 0.0001)
    # mask2 = (depth2 > 0.0001)

    masked_depth1 = depth1[mask1]
    masked_depth2 = depth2[mask2]

    # all_depth1 = np.concatenate((all_depth1, masked_depth1), axis=0)
    # all_depth2 = np.concatenate((all_depth2, masked_depth2), axis=0)

    # # plot the transit depths
    # # note the range!    j
    # ax.hist([masked_depth1, masked_depth2], bins=binsize, histtype='bar', 
            # normed=False, lw=1, color=['green', 'blue'], 
            # alpha=0.1, range=[0, 0.1], label=['primary eclipse', 'secondary eclipse'], 
            # log=False)

    # plot the transit depths
    # note the range!    j
    ax.hist([masked_depth1, masked_depth2], bins=binsize, histtype='bar', 
            normed=False, lw=1, color=['green', 'blue'],  
            alpha=0.1, range=[0, 0.1],  
            log=False)

# In[]:


plot_filename = "final5"

ax.legend( ('Primary Eclipses', 'Secondary Eclipses' ) )

ax.grid(True, which='minor')

ax.set_xlabel('apparent transit depth', fontsize=20, labelpad=10)
ax.set_ylabel('Number of stars', fontsize=16, labelpad=10)

ax.set_xlim([0,0.001])
ax.xaxis.set_major_locator(MultipleLocator(0.0001))
ax.tick_params(axis='y', which='major', labelsize=15)
ax.tick_params(axis='x', which='major', labelsize=15)
# ax.set_xlim([0,0.01])
# ax.xaxis.set_major_locator(MultipleLocator(0.001))

# ax.set_ylim([0,50])

fig.tight_layout()
# save the plot to disk
utils.save_plot(fig, plot_folder, plot_filename, show=False)

# next 2 lines just for testing purposes
# plt.show()






