

# In[]:

import os, sys

from astropy.io         import ascii as ascii_file
from astropy.table      import Table
from matplotlib.ticker  import MultipleLocator, FormatStrFormatter

import seaborn      as sns
import numpy        as np
import matplotlib
import matplotlib.pyplot as plt


# my handy utilities
sys.path.append('../')
from common  import utils


# In[]:

blend_header=[]
blend_header.append('star_id')
blend_header.append('star_mass')
blend_header.append('star_luminosity')
blend_header.append('binary_id')
blend_header.append('binary_evol')
blend_header.append('binary_mass1')
blend_header.append('binary_mass2')
blend_header.append('star_mag_r')
blend_header.append('binary_mag_r')
blend_header.append('delta_mag')
blend_header.append('Period')
blend_header.append('a')
blend_header.append('transit_probability')
blend_header.append('binary_radius1')
blend_header.append('binary_radius2')
blend_header.append('binary_luminosity1')
blend_header.append('binary_luminosity2')
blend_header.append('actual_transit depth 1')
blend_header.append('actual_transit depth 2')
blend_header.append('measured_transit_depth_1')
blend_header.append('measured_transit_depth_2')

# In[]:

base_folder = '/padata/beta/users/efarrell/data/plato'

# data_folder = base_folder + '/blend_output/run_3' # imf kroupka
# data_folder = base_folder + '/blend_output/run_4' # new jktepbop sufrace brightness
# data_folder = base_folder + '/blend_output/run_5' # 20 simulations
data_folder = base_folder + '/blend_output/run_4' # 1x1 deg 


grid_square = '16'

# load files names with simulated data
# into a dictionary object
pattern     = os.path.join(data_folder, grid_square, "all_blends*")
sim_files   = utils.load_files(glob_pattern=pattern, headers=blend_header)




# In[]:

# where will we save the plots to?
plot_folder = 'plots'
utils.rotate_folder(plot_folder)


# In[]:

# histogram of transit depths
# for each simulated blend file

for input_file in sim_files:

    fig = plt.figure(figsize=(17, 10))
    ax = plt.subplot(111)

    # read in the simulation data
    sim_data  = ascii_file.read(input_file, delimiter=' ')

    # binsize=500
    binsize=1000
    # binsize=50
    # binsize=20 # for 2nd eclipse


    plot_filename = os.path.splitext(os.path.basename(input_file))[0]
    print "processing: " + plot_filename 


    # ax.set_title('Blended transit depths: Latitude 50   to magnitude 26, new jktebop calcs (surface brightness)')
    # ax.set_title('Difference between primary and secondary eclipse depth:  Blends at Latitude 50   (to magnitude 26) (new jkt surface brightness)')


    depth1 = sim_data['measured_transit_depth_1'].data
    depth2 = sim_data['measured_transit_depth_2'].data

    mask1 = (depth1 > 0)
    mask2 = (depth2 > 0)

    # mask1 = (depth1 > 0.0001)
    # mask2 = (depth2 > 0.0001)

    masked_depth1 = depth1[mask1]
    masked_depth2 = depth2[mask2]


    # plot the transit depths
    # note the range!
    ax.hist([masked_depth1, masked_depth2], bins=binsize, histtype='bar', 
            normed=False, lw=1, color=['green', 'blue'], 
            alpha=0.5, range=[0, 0.1], label=['primary eclipse', 'secondary eclipse'], 
            log=False)

    # # plot the difference between transit depth 1 and depth 2
    # difference = np.abs(depth2 - depth1)
    # ax.hist(difference, bins=100, histtype='bar', normed=False, lw=1,  
            # alpha=0.4, label='blend eclipse depth difference', 
            # color='red', 
            # log=True)

    handles, labels = ax.get_legend_handles_labels()
    legend = ax.legend(handles, labels, loc='upper right', numpoints=3, frameon=True, framealpha=1, fancybox=True, fontsize='x-large', shadow=True)
    ax.grid(True, which='minor')

    ax.set_xlabel('apparent transit depth', fontsize=20, labelpad=10)
    ax.set_ylabel('Number of stars', fontsize=16, labelpad=10)

    ax.set_xlim([0,0.001])
    ax.xaxis.set_major_locator(MultipleLocator(0.0001))
    ax.tick_params(axis='y', which='major', labelsize=15)
    ax.tick_params(axis='x', which='major', labelsize=15)
    # ax.set_xlim([0,0.01])
    # ax.xaxis.set_major_locator(MultipleLocator(0.001))

    # ax.set_ylim([0,50])

    fig.tight_layout()
    # save the plot to disk
    utils.save_plot(fig, plot_folder, plot_filename, show=False)

    # next 2 lines just for testing purposes
    # plt.show()
    # break



# In[]:
# cumulative distribution method 1
# based on example from stack overflow

fig, ax = plt.subplots()


ax.set_title('Cumulative Distribution: Latitude 50 (square 78) - Down to magnitude 26 ')
# ax.set_title('Cumulative Distribution: Latitude 50   (square 78) Down to magnitude 26 MASK ZERO VALUES')


depth1 = sim_data['measured_transit_depth_1'].data
depth2 = sim_data['measured_transit_depth_2'].data

data = np.concatenate((depth1, depth2), axis=0)

sorted_data = np.sort(data)  # Or data.sort(), if data can be modified

# Cumulative distributions:
plt.step(sorted_data, np.arange(sorted_data.size))  # From 0 to the number of data points-1
# plt.step(sorted_data[::-1], np.arange(sorted_data.size))  # From the number of data points-1 to 0

ax.set_xlim([-0.1, 1])
# ax.set_xlim([0,0.02]) # for 2nd eclipse

ax.set_ylim([0,110000])

plt.show()



# In[]:

# cumulative distribution method 2
# based on example from stack overflow

fig, ax = plt.subplots()

ax.set_title('Cumulative Distribution: Latitude 50 (square 78) - Down to magnitude 26 ')
# ax.set_title('Cumulative Distribution: Latitude 50   (square 78) Down to magnitude 26 MASK ZERO VALUES')

depth1 = sim_data['measured_transit_depth_1'].data
depth2 = sim_data['measured_transit_depth_2'].data

data = np.concatenate((depth1, depth2), axis=0)

values, base = np.histogram(data, bins=100)

# evaluate the cumulative
cumulative = np.cumsum(values)

# plot the cumulative function
plt.plot(base[:-1], cumulative, c='blue')


plt.show()

# In[]:




