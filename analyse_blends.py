# -*- coding: utf-8 -*-


# In[]:

import os, sys
import getopt
import glob
import numpy                as np

from collections            import OrderedDict
from astropy.io             import ascii as ascii_file
from astropy.table          import Table

import matplotlib.pyplot    as plt
import matplotlib.cm        as cm
from matplotlib.ticker      import MultipleLocator, FormatStrFormatter

# my common functions - 
# assume 'common' folder is in path above this one
sys.path.append('../')
from common import utils
                     
                     
# In[]:

simulation_headers=['line_num', 'biseps_id', 
                    'lon', 'lat', 'r',
                    'mt1', 'mt2', 'reff1', 'reff2', 'lum1', 'lum2', 
                    'a', 'Porb', 'transit_prob', 
                    'transit_depth1', 'transit_depth2', 'inclin', 'met', 'evol']


# In[]:

# # When debugging:
# # Set 'run_folder' here
# run_folder = '/padata/beta/users/efarrell/data/github_plato/blend_output/run_7'


# When running from cluster job:
# Get 'run_folder' from command line 
opts, extra  = getopt.getopt(sys.argv[1:],'')
run_folder   = str(extra[0])


# double-check input parameters
if not os.path.exists(run_folder):
    print "Invalid parameter! Cannot find run_folder: " + run_folder
    print "Exiting program..."
    sys.exit()



# In[]:


run_subdirs =  utils.get_subdirs(run_folder)
squares     =  utils.filter_list(run_subdirs, remove='cluster_msgs')


# sort the sub-directories 
# in numerical order
int_squares = map(int, squares)
int_squares.sort()
squares = map(str, int_squares)

print run_subdirs
print squares

stats_filename='stats_allblends'
hist_filename='hist_totals_allblends'


# In[]:


# temp testing code
# squares = ['16']


# In[]:

# Process every square in the grid,
# calculating the MEAN VALUE for each blended transit depth

for square in squares:
    print "processing square: " + str(square)

    find_files    = 'all_blends'
    square_folder = os.path.join(run_folder, square)
    pattern       = os.path.join(square_folder, find_files + "*")


    # search for the name of every 'all_blends' file,
    # and store name in simulations dictionary
    simulations = {}
    file_list   =  glob.glob(pattern)
    file_list.sort()


    # load each simulated data file
    # into a dictionary object
    for file_name in file_list:
        print "reading file: " + file_name
        simulations[file_name] = ascii_file.read(file_name, delimiter=' ')


    # Summarise the blend depths 
    # from each run in a histogram
    # and write the histogram values
    # out to a CSV file...
    out_stats_file = os.path.join(square_folder, '.'.join([stats_filename, square, 'csv']))
    out_hist_file  = os.path.join(square_folder, '.'.join([hist_filename, square, 'csv']))


    # now create HISTOGRAM from each simulation file
    # and get mean and standard deviation of each Histogram

    # Setup histogram parameters
    num_bins        = 500
    end_bin_range   = 0.5
    bin_steps       = 0.001

    # num_bins        = 50
    # end_bin_range   = 0.05
    # bin_steps       = 0.01


    # create x axis
    x_ticks = np.arange(0, end_bin_range, bin_steps)


    # create MASTER ARRAY 
    # to hold each simulation run
    all_hists = np.zeros(shape=(0, num_bins), dtype='int')


    for sim_filename, sim_data in simulations.iteritems():
        print "processing: " + sim_filename

        # ----------
        # Key point!
        # ----------
        depth1 = sim_data['measured_transit_depth_1'].data
        depth2 = sim_data['measured_transit_depth_2'].data

        # combine both 'depth' arrays into one array
        x = np.concatenate((depth1, depth2), axis=0)

        # print "length depth1: " + str(len(depth1))
        # print "length depth2: " + str(len(depth2))
        # print "length x: " + str(len(x))

        # generate a histogram
        n, edges = np.histogram(x, bins=num_bins, range=[0, end_bin_range])

        # add the new histogram
        # to the master array
        all_hists = np.vstack([all_hists, n])


    # Write to txt file:
    # Each row has 'num_bins' columns,
    # one column, for each transit depth
    with open(out_hist_file,'w') as f:
        np.savetxt(f, all_hists, fmt=['%d'] * num_bins, delimiter=",")


    # calculate the mean and standard deviation of each histogram COLUMN 
    # ('axis=0' tells numpy to take the mean of the columns not the rows)
    sim_mean    = np.mean(all_hists, axis=0)
    sim_std     = np.std(all_hists, axis=0)
    sim_std_log = np.ma.log10(sim_std)


    # Write to txt file:
    # write out the 'mean' and 'standard deviation' 
    # for each blend depth
    with open(out_stats_file,'w') as f:
        np.savetxt(f, 
                   zip(x_ticks, sim_mean, sim_std, sim_std_log), 
                   fmt=['%5.3f', '%d', '%6.4f', '%6.4f'], 
                   delimiter=",")


# In[]:

