

# In[]:

import os, sys

from astropy.io         import ascii as ascii_file
from astropy.table      import Table
from matplotlib.ticker  import MultipleLocator, FormatStrFormatter

import seaborn      as sns
import numpy        as np
import matplotlib
import matplotlib.pyplot as plt


# my handy utilities
sys.path.append('../')
from common  import utils


# In[]:

blend_header=[]
blend_header.append('star_id')
blend_header.append('star_mass')
blend_header.append('star_luminosity')
blend_header.append('binary_id')
blend_header.append('binary_evol')
blend_header.append('binary_mass1')
blend_header.append('binary_mass2')
blend_header.append('star_mag_r')
blend_header.append('binary_mag_r')
blend_header.append('delta_mag')
blend_header.append('Period')
blend_header.append('a')
blend_header.append('transit_probability')
blend_header.append('binary_radius1')
blend_header.append('binary_radius2')
blend_header.append('binary_luminosity1')
blend_header.append('binary_luminosity2')
blend_header.append('actual_transit depth 1')
blend_header.append('actual_transit depth 2')
blend_header.append('measured_transit_depth_1')
blend_header.append('measured_transit_depth_2')

# In[]:

base_folder = '/padata/beta/users/efarrell/data/plato'

# data_folder = base_folder + '/blend_output/run_3' # imf kroupka
# data_folder = base_folder + '/blend_output/run_4' # new jktepbop sufrace brightness
# data_folder = base_folder + '/blend_output/run_5' # 20 simulations
data_folder = base_folder + '/blend_output/run_4' # 1x1 deg 


grid_square = '16'

# load files names with simulated data
# into a dictionary object
pattern     = os.path.join(data_folder, grid_square, "all_blends*")
sim_files   = utils.load_files(glob_pattern=pattern, headers=blend_header)



# In[]:

# where will we save the plots to?
plot_folder = 'plots'
utils.rotate_folder(plot_folder)



# In[]:
# cumulative distribution method 1
# based on example from stack overflow

fig, ax = plt.subplots()


ax.set_title('Cumulative Distribution: Latitude 50 (square 78) - Down to magnitude 26 ')
# ax.set_title('Cumulative Distribution: Latitude 50   (square 78) Down to magnitude 26 MASK ZERO VALUES')


depth1 = sim_data['measured_transit_depth_1'].data
depth2 = sim_data['measured_transit_depth_2'].data

data = np.concatenate((depth1, depth2), axis=0)

sorted_data = np.sort(data)  # Or data.sort(), if data can be modified

# Cumulative distributions:
plt.step(sorted_data, np.arange(sorted_data.size))  # From 0 to the number of data points-1
# plt.step(sorted_data[::-1], np.arange(sorted_data.size))  # From the number of data points-1 to 0

ax.set_xlim([-0.1, 1])
# ax.set_xlim([0,0.02]) # for 2nd eclipse

ax.set_ylim([0,110000])

plt.show()



# In[]:

# cumulative distribution method 2
# based on example from stack overflow

fig, ax = plt.subplots()

ax.set_title('Cumulative Distribution: Latitude 50 (square 78) - Down to magnitude 26 ')
# ax.set_title('Cumulative Distribution: Latitude 50   (square 78) Down to magnitude 26 MASK ZERO VALUES')

depth1 = sim_data['measured_transit_depth_1'].data
depth2 = sim_data['measured_transit_depth_2'].data

data = np.concatenate((depth1, depth2), axis=0)

values, base = np.histogram(data, bins=100)

# evaluate the cumulative
cumulative = np.cumsum(values)

# plot the cumulative function
plt.plot(base[:-1], cumulative, c='blue')


plt.show()

# In[]:




