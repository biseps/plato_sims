# -*- coding: utf-8 -*-

# In[]:

import os, sys
import glob
import numpy as np
from collections import OrderedDict

from astropy.io import ascii as ascii_file
from astropy.table import Table


import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator, FormatStrFormatter
import matplotlib.cm as cm


# my common functions - assume common folder is in path above this one
sys.path.append('../')
from common import utils
                     


# In[]:


headers=['id', 'long', 'lat', 'u', 'g', 'r', 'i', 
         'z', 'd51', 'jj', 'jk', 'jh', 'kp'] 

# In[]:

base_folder = '/home/physastro/efarrell/rob_field79'
stars_young_file = os.path.join(base_folder, 'pop_s_y/79/mag.dat.17')
stars_old_file   = os.path.join(base_folder, 'pop_s_o/79/mag.dat.17')

stars_young = ascii_file.read(stars_young_file, names=headers)
stars_old = ascii_file.read(stars_old_file, names=headers)

stars = np.concatenate((stars_young, stars_old), axis=0)

plot_bands = ['u', 'g', 'r', 'i', 'z', 'kp']


# In[]:

fig = plt.figure(figsize=(12, 8))
ax = plt.subplot(111)


# ax.set_xlim([0, 100])
# ax.set_ylim([0, 100])

ax.set_title('ROB FARMER ~51,000 Single Stars, Kepler square 79', fontsize=16, fontweight='bold')
ax.set_xlabel('Magnitude ', fontsize=16, fontweight='bold')
ax.set_ylabel('number of stars',     fontsize=16, fontweight='bold')


for passband_name in plot_bands:
    print passband_name
    data = stars[passband_name]
    ax.hist(data, bins=100, histtype='step', normed=False, lw=2, 
            alpha=0.6,  label=passband_name, log=False)


handles, labels = ax.get_legend_handles_labels()
ax.legend(handles, labels)

# ax.minorticks_on() 
# ax.grid(True, which='minor')
ax.grid(True, which='major')
# ax.grid(True, which='both')


plt.show()

# In[]:
