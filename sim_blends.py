# -*- coding: utf-8 -*-


# In[]:

import os, sys, time, shutil
import math
import getopt
import logging


# my common functions - 
# assume 'common' folder is in path above this one
sys.path.append('../')
from common import utils
from common import constants as c


from astropy.coordinates import ICRS, Galactic
from astropy.coordinates import match_coordinates_sky
from astropy import units as u
from astropy.io import ascii as ascii_file

import astropy.table
import astropy.constants as const


import numpy as np
from random import randint

import matplotlib
import matplotlib.pyplot as plt




# In[]:

# constants
DATABASE_TABLE  = 'depths'


# # Code only for manual debugging
# grid_square     = '16'
# output_dir      = './blends'
# cluster_task_id = '8'
# plato_folder    = '/padata/beta/users/efarrell/data/github_plato/pop_output/field1/run_5'
# db_file         = '/padata/beta/users/efarrell/data/github_plato/jkt_output/run_5/' + grid_square + '/depths_' + grid_square + '.sqlite'




# Get command line arguments
opts, extra  = getopt.getopt(sys.argv[1:],'')
grid_square  = str(extra[0])
plato_folder = str(extra[1])
db_file      = str(extra[2])
output_dir   = str(extra[3])
cluster_task_id  = str(extra[4])


# setup file for debug messages
utils.set_logging_config(logging, log_level=logging.INFO, filebase=os.path.basename(db_file))
                
                
# debugging
logging.info('%s', grid_square)
logging.info('%s', plato_folder)
logging.info('%s', db_file)
logging.info('%s', output_dir)


# In[]:


def porb2a(m1, m2, porb):
    """ Calculate semi-major axis

        Porb      is in years
        m1, m2    are in solar masses
        a         is in solar radii

    """

    n = 2.0 * np.pi / (porb/365.0)

    return (c.gn * (m1+m2) / n**2)**(1.0/3.0)



def write_blend_info(filename, stars, binaries, eclipse):

    fields=[]
    fields.append(stars['id'])
    fields.append(stars['mt1'])
    fields.append(stars['lum1'])
    fields.append(binaries['biseps_id'])
    fields.append(binaries['evol'])
    fields.append(binaries['mt1'])
    fields.append(binaries['mt2'])
    fields.append(stars['r'])
    fields.append(binaries['r'])
    fields.append(eclipse['delta_mag'])
    fields.append(binaries['Porb'])
    fields.append(eclipse['a'])
    fields.append(eclipse['transit_prob'])
    fields.append(binaries['reff1'])
    fields.append(binaries['reff2'])
    fields.append(binaries['lum1'])
    fields.append(binaries['lum2'])
    fields.append(eclipse['transit_depth1_actual'])
    fields.append(eclipse['transit_depth2_actual'])
    fields.append(eclipse['transit_depth1_measured'])
    fields.append(eclipse['transit_depth2_measured'])


    header=[]
    header.append('star_id')
    header.append('star_mass')
    header.append('star_luminosity')
    header.append('binary_id')
    header.append('binary_evol')
    header.append('binary_mass1')
    header.append('binary_mass2')
    header.append('star_mag_r')
    header.append('binary_mag_r')
    header.append('delta_mag')
    header.append('Period')
    header.append('a')
    header.append('transit_probability')
    header.append('binary_radius1')
    header.append('binary_radius2')
    header.append('binary_luminosity1')
    header.append('binary_luminosity2')
    header.append('actual_transit depth 1')
    header.append('actual_transit depth 2')
    header.append('measured_transit_depth_1')
    header.append('measured_transit_depth_2')


    # create astropy table object
    data = astropy.table.Table(fields, names=header)

    # write out table object
    ascii_file.write(data, filename, delimiter=' ')





def fluxToMag_astropyhics_module(flux):
    a = -2.5/np.log(10.0)
    return a * np.log(flux)


def fluxToMag(flux, system="Relative"):
    """Convert flux to magnitude.

    """
    if system == "AB":
        mag = (-2.5) * np.log10(flux) + 8.926

    elif system == "Relative":
        mag = (-2.5) * np.log10(flux)

    else:
        mag=None

    return mag



def magToFlux(mag, system="Relative"):
    """Convert magnitude to flux.

    """
    if system == "AB":
        # For the AB magnitude system, a fixed 
        # zero-point-flux is used as a reference
        # instead of the star Vega.
        # Output flux is in Janskys.
        zero_point_flux = 3631 
        flux = (0.3981)**mag * zero_point_flux

    elif system == "Relative":
        flux = (10)**((-0.4) * mag) 

    else:
        flux=None

    return flux



def addMags(mag1, mag2):
    # Taken from: 
    # John Southward PDF
    expr1       = 10**((-0.4) * mag1)
    expr2       = 10**((-0.4) * mag2)
    total_mag   = (-2.5) * np.log10(expr1 + expr2) 
    return total_mag



# telescope on-sky pixel size
PLATO_plate_scale = 15 * u.arcsec;


# define region where blend is likely
blend_region = PLATO_plate_scale 



# In[]:

# '_s_o'  = stars old
# '_s_y'  = stars young
# '_b_o'  = binaries old
# '_b_y'  = binaries young


# Get MAGNITUDES 
# from "mag.dat" files
filename = 'mag.dat.1'

mag_cols = ['id', 'lon', 'lat', 'u', 'g', 'r', 'i', 'z', 'd51', 'jj', 'jk', 'jh', 'kp']

mag_s_o  = ascii_file.read(os.path.join(plato_folder, 'pop_s_o', grid_square,  filename), delimiter=' ', names=mag_cols)
mag_s_y  = ascii_file.read(os.path.join(plato_folder, 'pop_s_y', grid_square,  filename), delimiter=' ', names=mag_cols)
mag_b_o  = ascii_file.read(os.path.join(plato_folder, 'pop_b_o', grid_square,  filename), delimiter=' ', names=mag_cols)
mag_b_y  = ascii_file.read(os.path.join(plato_folder, 'pop_b_y', grid_square,  filename), delimiter=' ', names=mag_cols)


# In[]:


# Get stellar PROPERTIES 
# from extract files
filename = 'extract.1'

extract_cols = ['line_num', 'biseps_id', 'mt1', 'reff1', 'teff1', 'lum1', 'mt2', 'reff2', 'teff2', 'lum2', 
                'Porb', 'birth', 'death', 'massTran', 'kw1', 'kw2', 'bkw', 'met', 'evol']

extract_s_o  = ascii_file.read(os.path.join(plato_folder, 'pop_s_o', grid_square,  filename), delimiter=' ', names=extract_cols)
extract_s_y  = ascii_file.read(os.path.join(plato_folder, 'pop_s_y', grid_square,  filename), delimiter=' ', names=extract_cols)
extract_b_o  = ascii_file.read(os.path.join(plato_folder, 'pop_b_o', grid_square,  filename), delimiter=' ', names=extract_cols)
extract_b_y  = ascii_file.read(os.path.join(plato_folder, 'pop_b_y', grid_square,  filename), delimiter=' ', names=extract_cols)



# In[]:

# combine young and old info
star_mags     = astropy.table.vstack([mag_s_o, mag_s_y])
star_props    = astropy.table.vstack([extract_s_o, extract_s_y])

binary_mags   = astropy.table.vstack([mag_b_o, mag_b_y])
binary_props  = astropy.table.vstack([extract_b_o, extract_b_y])


# Finally combine magnitudes and properties 
# into single tables for easier access
stars    = astropy.table.hstack([star_mags, star_props])
binaries = astropy.table.hstack([binary_mags, binary_props])


# In[]:


# convert coordinates into astropy units

star_coords     = Galactic(stars['lon'], 
                           stars['lat'], 
                           unit=(u.degree, u.degree))

binary_coords   = Galactic(binaries['lon'], 
                          binaries['lat'], 
                          unit=(u.degree, u.degree))



# In[]:


# -----------------------NOT USED-------------------------------------------------------------
# get 4 closest binaries

# closest_index=[]
# dists_2d=[]
# dists_3d=[]

# for i in range(1,5):
    # idx, 2d_distance, d3d = match_coordinates_sky(star_coords, binary_coords, nthneighbor=i, storekdtree=True)

    # closest_index.append(idx)
    # dists_2d.append(2d_distance)
    # dists_3d.append(d3d)
# -----------------------NOT USED-------------------------------------------------------------


# In[]:


# get nearest neighbour for each star
which_neighbour = 1
nearest_idx, dist_2d, dist_3d = match_coordinates_sky(star_coords, 
                                                      binary_coords, 
                                                      nthneighbor=which_neighbour, 
                                                      storekdtree=True)


# In[]:


# -----------------------NOT USED-------------------------------------------------------------

# create array of closest binaries
# close_binary[0] = 1st closest binary array
# close_binary[1] = 2st closest binary array
# etc etc..

# close_binary=[]

# for i in range(0,4):
    # close_binary.append(binary_coords[nearest_idx[i]])
# -----------------------NOT USED-------------------------------------------------------------


# In[]:




# re-index the "binaries" array 
# into "nearest neighbour" order
closest_binary        = binaries[nearest_idx]


# Match all binaries that fall INSIDE 
# the blend region which means they are 
# close to a foreground star
mask = (dist_2d.arcsecond < blend_region.value)


blend_stars     = stars[mask]
blend_binaries  = closest_binary[mask]


# print "Nearest Neighbour: " + str(which_neighbour)
# print "number of blend_stars " + str(len(blend_stars))
# print "number of blend_binaries " + str(len(blend_binaries))
# print "------"


# In[]:


# Discard binaries that are greater than
# 10 magnitudes fainter...they wont create a blend
delta_mag = blend_binaries['r'].data - blend_stars['r'].data

mask             = (delta_mag < 10)

blend_stars      = blend_stars[mask]
blend_binaries   = blend_binaries[mask]
delta_mag        = delta_mag[mask]



# In[]:

# Convert Period into semi-major axis
a  = porb2a(blend_binaries['mt1'].data, 
            blend_binaries['mt2'].data, 
            blend_binaries['Porb'].data) 




# In[]:


# geometric probability of transit
radius1 = blend_binaries['reff1'].data
radius2 = blend_binaries['reff2'].data

transit_prob = (radius1 + radius2) / a




# In[]:


# work out ACTUAL fractional eclipse depth
# for each binary system
transit_depth1 = []
transit_depth2 = []


# create a random inclination
# for every binary system we have
random_inclinations = np.random.randint(low=0, high=90, size=len(blend_binaries))


# load eclipse depths 
# database into memory
database = utils.sqlite_to_memory(db_file)

logging.info('length binary table: %s', str(len(blend_binaries)))
row_counter = 0


# In[]:

with database:

    cur = database.cursor()    


    # loop through every binary system 
    # in the 'blend_binaries' dataset
    for (binary, random_i) in zip(blend_binaries, random_inclinations):

        row_counter += 1
        logging.debug("Row: %s", str(row_counter))
        logging.debug('Searching for: %s, %s, %s ', binary['biseps_id'], random_i, binary['met'])


        # Using a random inclination angle,
        # read the database to find out what eclipse depth
        # the current binary system will show for this inclination

        SQL_STRING  = 'select trans_depth1, trans_depth2 '
        SQL_STRING += 'from {table} '
        SQL_STRING += 'where id     = {biseps_id} ' 
        SQL_STRING += 'and   inclin = {random_i} '
        SQL_STRING += 'and   z      = {metallicity} '
        SQL_STRING += 'limit 1 '

        query = SQL_STRING.format(table        = DATABASE_TABLE, 
                                  biseps_id    = binary['biseps_id'], 
                                  random_i     = random_i,
                                  metallicity  = binary['met'])


        results = cur.execute(query)


        # just get the first matching record
        result =  results.fetchone() 

        if result is None:
            # found no eclipse 
            # for this inclination!
            transit_depth1.append(0)
            transit_depth2.append(0)

        else:
            # save our eclipse depths 
            # from the jktebop database
            transit_depth1.append(result[0])
            transit_depth2.append(result[1])



# In[]:

# convert eclipse depth 'lists'
# into numpy arrays
transit_depth1_actual = np.asarray(transit_depth1)
transit_depth2_actual = np.asarray(transit_depth2)



# In[]:


# calculate MEASURED fractional transit depth
# as per R. Farmer 1st year report
transit_depth1_measured = transit_depth1_actual / (1 + 10**(delta_mag/2.5))
transit_depth2_measured = transit_depth2_actual / (1 + 10**(delta_mag/2.5))



# In[]:

# convert all the eclipse data into a 'astropy table'
# (it will be written out to a file later)

eclipse_info=[]
eclipse_info.append(delta_mag)
eclipse_info.append(a)
eclipse_info.append(transit_prob)
eclipse_info.append(transit_depth1_actual)
eclipse_info.append(transit_depth2_actual)
eclipse_info.append(transit_depth1_measured)
eclipse_info.append(transit_depth2_measured)

eclipse_headers=[]
eclipse_headers.append('delta_mag')
eclipse_headers.append('a')
eclipse_headers.append('transit_prob')
eclipse_headers.append('transit_depth1_actual')
eclipse_headers.append('transit_depth2_actual')
eclipse_headers.append('transit_depth1_measured')
eclipse_headers.append('transit_depth2_measured')


eclipse_table = astropy.table.Table(eclipse_info, names=eclipse_headers)


# In[]:


# create an output folder 
utils.make_folder(output_dir)


# write out blend data files
out_filename = os.path.join(output_dir, 'all_blends.' + cluster_task_id + '.csv')
write_blend_info(out_filename, blend_stars,  blend_binaries, eclipse_table)


# In[]:


# create header row
headers = ['total_stars','total_binaries', 'potential_blends']
txt_headers= ', '.join(map(str, headers))


# convert totals 
# into a flat numpy array
totals  = [len(stars), len(binaries), len(blend_stars)]
data    = np.asarray(totals).reshape(-1, 3)


# write out data to file
out_filename = os.path.join(output_dir, 'totals.' + cluster_task_id + '.csv')
np.savetxt(out_filename, data, fmt='%d', header=txt_headers, delimiter="," )



# In[]:

