

# In[]:

import os, sys

import numpy as np
from astropy.io import ascii as ascii_file
from astropy.table import vstack
from astropy.table import Table
from astropy.table import join
import astropy.constants as const

import matplotlib
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator, FormatStrFormatter
import matplotlib.cm as cm
from matplotlib.colors import LogNorm
from matplotlib.ticker import MultipleLocator, FormatStrFormatter


# my common functions - assume common folder is in path above this one
sys.path.append('../')
from common import utils


# import seaborn as sns



# In[]:

# data_dir='/padata/beta/users/efarrell/data/ngts_fields/11_run/field1/blends_5'

print "before data dir"
# data_dir = '/padata/beta/users/efarrell/data/plato/sim_transits/code/blends'
data_dir = './'

# input_file = os.path.join(data_dir, 'planet_like.1.csv')
# input_file = os.path.join(data_dir, 'all_blends.1.csv')
input_file = os.path.join(data_dir, 'summary_78.csv')

file_data  = ascii_file.read(input_file, delimiter=',')


# runs = np.arange(0,100)

# In[]:

fig = plt.figure(figsize=(10, 15))

binsize=100
# binsize=20 # for 2nd eclipse

# bartype='step'
bartype='bar'
# do_normed=True
do_normed=False

# data1=file_data['diff'].data 
data_abs = np.abs(file_data['diff'].data)
data1=data_abs

# data2=file_data['a'].data

# data1=file_data['actual_transit_depth_1'].data
# data2=file_data['actual_transit_depth_2'].data

# data1=file_data['measured_transit_depth_1'].data
# data2=file_data['measured_transit_depth_2'].data

ax = plt.subplot(111)
h2 = ax.hist(data1, bins=binsize, histtype=bartype, normed=do_normed, lw=1, color='green', alpha=0.6, range=[0, 0.05], label='difference', log=True)
# h3 = ax.hist(data2, bins=binsize, histtype=bartype, normed=do_normed, lw=1, color='blue', alpha=0.5, range=[0, 0.05], label='2nd (secondary) eclipse', log=True)

# h4 = ax.hist([data1, data2], bins=binsize, histtype=bartype, normed=do_normed, lw=1, color=['green', 'blue'], alpha=0.5, range=[0, 0.05], label=['primary eclipse', 'secondary eclipse'], log=True)

# h3 = ax.hist(data2, bins=binsize, histtype=bartype, normed=do_normed, lw=3, color='blue', alpha=0.3, range=[0, 0.01], label='2nd (secondary) eclipse')

ax.set_title('Analyse eclipse depths')

handles, labels = ax.get_legend_handles_labels()
ax.legend(handles, labels)
ax.grid(True, which='minor')

ax.set_xlabel('difference between 1st/2nd eclipse depth', fontsize=13, fontweight='bold', labelpad=20)
ax.set_ylabel('Number of systems', fontsize=13, fontweight='bold', labelpad=20)

# ax.set_xlim([-0.1, 0.1])
# ax.set_xlim([0,0.02]) # for 2nd eclipse

# ax.set_ylim([0,900])

plt.show()

# In[]:

# # scatter plot

# ax = plt.subplot(111)
# # ax.set_title(filename)
# ax.scatter(file_data['bin_luminosity1'].data, file_data['bin_luminosity2'].data, s=15, lw=0, alpha=0.7)

# ax.set_xlim([0,40])
# ax.set_ylim([0,5])

# ax.set_title('characteristics of binaries producing planet-like transits')
# ax.grid(True, which='minor')
# ax.set_xlabel('luminosity binary 1 (solar luminosities)', fontsize=13)
# ax.set_ylabel('luminosity binary 2', fontsize=13)

# plt.show()
# # save_plot(fig, '.', 'probs.jpg')


# In[]:


# comapre mass 1 and mass 2 of binary system

fig, ax = plt.subplots()


x = file_data['mt1'].data
y = file_data['mt2'].data

ax.set_title('Compare masses ', fontsize=16, fontweight='bold')
ax.set_xlabel('mass 1', fontsize=16, fontweight='bold')
ax.set_ylabel('mass 2',        fontsize=16, fontweight='bold')

# x_log10 = np.log10(x)
# x_log10 = np.ma.log10(x) # mask invalid values
# y_log10 = np.ma.log10(y) # mask invalid values


# _, _, _, hist_image = ax.hist2d(x_log10, y_log10, bins=[50,50])
_, _, _, hist_image = ax.hist2d(x, y, bins=[1000,1000])

ax.set_xlim([0.09, 0.15])
ax.set_ylim([0.09, 0.15])

fig.colorbar(hist_image, ax=ax)


plt.show()


# In[]:

