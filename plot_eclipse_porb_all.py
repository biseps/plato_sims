# -*- coding: utf-8 -*-


# In[]:

import os, sys
import glob
import numpy                as np

from collections            import OrderedDict

from astropy.io             import ascii as ascii_file
from astropy.table          import Table


import matplotlib.pyplot    as plt
import matplotlib.cm        as cm
from matplotlib.ticker      import MultipleLocator, FormatStrFormatter
from matplotlib.colors      import LogNorm

# my common functions - assume common folder is in path above this one
sys.path.append('../')
from common import utils
                     


# In[]:

simulation_headers=['line_num', 'biseps_id', 
                    'lon', 'lat', 'r',
                    'mt1', 'mt2', 'reff1', 'reff2', 'lum1', 'lum2', 
                    'a', 'Porb', 'transit_prob', 
                    'eclipse_depth1', 'eclipse_depth2', 'inclin', 'met', 'evol']



# In[]:


# Get all transit simulation files
# for an individual square

# run_folder = '../output/run_90'
# run_folder = '/padata/beta/users/efarrell/data/plato/eclipse_output/run_2'
# run_folder = '/padata/beta/users/efarrell/data/plato/eclipse_output/run_5/16'
run_folder = '/padata/beta/users/efarrell/data/plato/eclipse_output/run_5'

run_subdirs =  utils.get_subdirs(run_folder)
squares     =  utils.filter_list(run_subdirs, remove='cluster_msgs')

# lots of messing to get sub-directories sorted
int_squares = map(int, squares)
int_squares.sort()
squares = map(str, int_squares)


print run_subdirs
print squares


# In[]:

# testing code
# only load some code
# squares = ['6', '16', '26']
# squares = ['16']


# read all simulation files

x= np.array([], dtype=np.float64)
y= np.array([], dtype=np.float64)

for square in squares:
    print "current square is: " + str(square)

    current_folder = os.path.join(run_folder, square)
    pattern        = os.path.join(current_folder, "simulated.*")

    # load simulations from file
    simulations = utils.load_files(glob_pattern=pattern, headers=simulation_headers)


    for sim_filename, sim_data in simulations.iteritems():
        print "processing: " + sim_filename

        # ----------
        # Key point!
        # ----------
        # hmmm dont think this is right
        # shouldnt combine all simulations
        x = np.concatenate((x, sim_data['eclipse_depth1'].data), axis=0)
        y = np.concatenate((y, sim_data['Porb'].data), axis=0)
        break

        # # --------
        # # temp enda
        # # --------
        # # just take one simulation!
        # x = sim_data['eclipse_depth1'].data
        # y = sim_data['Porb'].data



# In[]:


plot_dir = './plots'
utils.rotate_folder(plot_dir)


# In[]:

# Plot a "log log" plot of eclipse depth v orbital period

fig, ax = plt.subplots()

period_label=r'$\log_{10}$ Period [days]'
eclipse_label=r'$\log_{10}$ Eclipse Depth'

# ax.set_title('??? ', fontsize=16, fontweight='bold')
ax.set_xlabel(eclipse_label, fontsize=16 )
ax.set_ylabel(period_label,  fontsize=16 )


# mask invalid values
# x_log10 = np.log10(x)
# y_log10 = np.log10(y)
mask = (x > 0)
x_mask = x[mask]
y_mask = y[mask]
x_log10 = np.ma.log10(x_mask) 
y_log10 = np.ma.log10(y_mask) 
# x_log10 = np.ma.log10(x) 
# y_log10 = np.ma.log10(y) 



# this is a good one for 20 simulated runs
_, _, _, hist_image = ax.hist2d(x_log10, y_log10, bins=[50,50], norm=LogNorm())
# _, _, _, hist_image = ax.hist2d(x_log10, y_log10, bins=[100,100], norm=LogNorm())
# _, _, _, hist_image = ax.hist2d(x_log10, y_log10, bins=[500,500], norm=LogNorm())

# these 2 are for 1 run
# _, _, _, hist_image = ax.hist2d(x_log10, y_log10, bins=[50,50], norm=LogNorm())
# _, _, _, hist_image = ax.hist2d(x_log10, y_log10, bins=[50,50])


# fig.colorbar(H[3], ax=ax)
# fig.colorbar(hist_image, ax=ax)
fig.colorbar(hist_image, ax=ax, ticks=MultipleLocator(10))


# ax.set_xlim([1, -10]) # log
ax.set_xlim([-10, 1]) # log
# ax.set_xlim([0, ]) # log

# ax.set_ylim([0, 500]) # days
# ax.set_ylim([0, 200]) # days
# ax.set_ylim([0, 100]) # days # good one

# ax.set_ylim([0, 2]) # years
# ax.set_ylim([0, 0.5]) # years

# ax.set_xscale('log')
# ax.set_yscale('log')


plt.show()

# In[]:


