

# In[]:

import os, sys, time, shutil

sys.path.append('/padata/beta/users/efarrell/repos/biseps/plot')
import biseps

from astropy.coordinates import ICRS, Galactic
from astropy.coordinates import match_coordinates_sky
from astropy import units as u

import numpy as np

import matplotlib
import matplotlib.pyplot as plt
plt.rcParams['axes.grid'] = True

import seaborn as sns

import random
from matplotlib.ticker import MultipleLocator, FormatStrFormatter

# In[]:


def get_lat_lon(file_path):
    mag_file = biseps.readFile()
    mag_file.appMag5(file_path)

    return mag_file.appMag['lat'], mag_file.appMag['long']



def save_plot(fig, folder, filename, img_format='jpg'):
    """@todo: Docstring for save_plot.

    :folder: @todo
    :filename: @todo
    :format: @todo
    :returns: @todo

    """
    fig.suptitle(folder + ' - ' + filename)
    fig.savefig(os.path.join(folder, filename + '.' + img_format), format=img_format)
    plt.show()
    plt.close(fig)


def plot_star_field(galactic_coords, titles, folder, filename):
    """@todo: Docstring for plotColorColor.

    :arg1: @todo
    :returns: @todo

    """
    fig = plt.figure(figsize=(20, 20))

    ax = plt.subplot(111)
    ax.set_title(folder + ' field')
    ax.grid(True, which='minor')
    ax.set_xlabel('lon', fontsize=13)
    ax.set_ylabel('lat', fontsize=13)

    for i, coords in enumerate[galactic_coords]:
        kwargs = dict(marker='o', s=5, color='red', lw=1, alpha=0.6)
        kwargs = dict(marker='o', s=5, color='green', lw=1, alpha=0.6)
        kwargs = dict(marker='+', s=95, color='gray', lw=1, alpha=0.7)
        kwargs = dict(marker='+', s=95, color='blue', lw=1, alpha=0.7)

        ax.scatter(coords.l, coords.b, label=titles[i],  **kwargs)

    # legend with some customizations.
    legend = ax.legend(loc='upper right', shadow=False)

    save_plot(fig, folder, filename)



# In[]:

grid_square = '16' # centre field, 1x1 deg

# base_dir = '/padata/beta/users/efarrell/plotting/ngts/input/ngts'
base_dir = '/padata/beta/users/efarrell/data/plato/pop_output/field1/run_5'

input_paths = []
input_paths.append(os.path.join(base_dir, 'pop_s_o', grid_square, 'mag.dat.1'))
input_paths.append(os.path.join(base_dir, 'pop_s_y', grid_square, 'mag.dat.1'))
input_paths.append(os.path.join(base_dir, 'pop_b_o', grid_square, 'mag.dat.1'))
input_paths.append(os.path.join(base_dir, 'pop_b_y', grid_square, 'mag.dat.1'))


# In[]:

lat_star_old, lon_star_old = get_lat_lon(input_paths[0])
lat_star_young, lon_star_young = get_lat_lon(input_paths[1])

lat_binary_old, lon_binary_old = get_lat_lon(input_paths[2])
lat_binary_young, lon_binary_young = get_lat_lon(input_paths[3])


# In[]:

# add the arrays together
lat_stars = np.concatenate((lat_star_old, lat_star_young), axis=0)
lon_stars = np.concatenate((lon_star_old, lon_star_young), axis=0)

lat_binary = np.concatenate((lat_binary_old, lat_binary_young), axis=0)
lon_binary = np.concatenate((lon_binary_old, lon_binary_young), axis=0)


# In[]:


# convert coordinate values into astropy units
# stars1 = Galactic(lon_star_old, lat_star_old, unit=(u.degree, u.degree))
# binaries1 = Galactic(lon_binary_old, lat_binary_old, unit=(u.degree, u.degree))

# stars1 = Galactic(lon_stars[:1], lat_stars[:1], unit=(u.degree, u.degree))
stars1 = Galactic(lon_stars, lat_stars, unit=(u.degree, u.degree))
binaries1 = Galactic(lon_binary, lat_binary, unit=(u.degree, u.degree))



# In[]:

closest_index=[]
dists_2d=[]
dists_3d=[]

for i in range(1,5):
    which_neighbour = i
    idx, d2d, d3d = match_coordinates_sky(stars1, binaries1, nthneighbor=which_neighbour, storekdtree=True)

    closest_index.append(idx)
    dists_2d.append(d2d)
    dists_3d.append(d3d)



# In[]:

# matches = binaries1[idx]

# dist_l = (matches.l - stars1.l).arcmin
# dist_b = (matches.b - stars1.b).arcmin

# In[]:

neighbour=[]

for i in range(0,4):
    neighbour.append(binaries1[closest_index[i]])



# In[]:

kwargs1 = dict(marker='o', s=5, color='red', lw=1, alpha=0.6)
kwargs2 = dict(marker='o', s=5, color='green', lw=1, alpha=0.6)
kwargs3 = dict(marker='+', s=95, color='gray', lw=1, alpha=0.7)
kwargs4 = dict(marker='+', s=95, color='blue', lw=1, alpha=0.7)
kwargs5 = dict(marker='+', s=95, color='red', lw=1, alpha=0.3)


fig = plt.figure(figsize=(15, 10))
ax = plt.subplot(111)
# ax.set_title(folder + ' field')
ax.grid(True, which='minor')


latlabel  = r'Galactic Latitude, b [degrees]'
longlabel = r'Galactic Longitude, l [degrees]'

ax.set_xlabel(longlabel, fontsize=15, fontweight='bold')
ax.set_ylabel(latlabel, fontsize=15, fontweight='bold')

# x_max = stars1.l.max().value
# y_max = stars1.b.max().value

idx_target_star = 50

x_max = stars1[idx_target_star].l.value
y_max = stars1[idx_target_star].b.value

ax.set_xlim([x_max-0.1, x_max+0.05])
ax.set_ylim([y_max-0.05, y_max+0.05])

# ax.set_xlim([x_max-1, x_max+1])
# ax.set_ylim([y_max-1, y_max+1])


# ax.scatter(stars1.l[:15].value, stars1.b[:15].value, label='old stars',  **kwargs1)
# ax.scatter(binaries1.l[:15].value, binaries1.b[:15].value, label='old_binaries',  **kwargs2)
# ax.scatter(matches.l[:15].value, matches.b[:15].value, label='matches',  **kwargs5)


ax.scatter(stars1.l.value, stars1.b.value, label='single star',  marker='o', s=50, color='red', lw=1, alpha=0.5)
ax.scatter(binaries1.l.value, binaries1.b.value, label='binary system',  marker='o', s=45, color='green', lw=1, alpha=0.6)

label  = 'Target Star'

x = stars1.l.value[idx_target_star]
y = stars1.b.value[idx_target_star]

plt.annotate(
    label,
    xy = (x, y), xytext = (20,90),
    textcoords = 'offset points', ha = 'right', va = 'bottom',
    bbox = dict(boxstyle = 'round,pad=0.5', fc = '#D65C5C', alpha = 1),
    arrowprops = dict(arrowstyle = '->', lw = 1.5, color = '#636363',  connectionstyle = 'arc3,rad=0'))


# put a circle around the target star
circle1 = plt.Circle((x, y), radius=0.007, color='#CC9900', alpha=0.3, fill=True)
ax.add_patch(circle1)


label  = 'Neighbour {which_neighbour}\n ({dist:5.3f}")'
labels = [label.format(which_neighbour=i+1, 
          dist=dists_2d[i][0].arcsec) 
          for i in range(len(neighbour))]

label_offset_x = [-110, 140, -60, 50]
label_offset_y = [-60, 38, 0, -70]

for i in range(0,4):
        x = neighbour[i].l.value[idx_target_star]
        y = neighbour[i].b.value[idx_target_star]

        plt.annotate(
            labels[i],
            xy = (x, y), xytext = (label_offset_x[i], label_offset_y[i]),
            textcoords = 'offset points', ha = 'right', va = 'bottom',
            bbox = dict(boxstyle = 'round,pad=0.5', fc = '#85AD85', alpha = 1),
            arrowprops = dict(arrowstyle = '->', lw = 1.5, color = '#636363', connectionstyle = 'arc3,rad=0'))



# legend with some customizations.
legend = ax.legend(loc='upper left', scatterpoints=3, frameon=True, framealpha=1, fancybox=True, fontsize='xx-large', shadow=True)

# The frame is matplotlib.patches.Rectangle instance surrounding the legend.
frame = legend.get_frame()
frame.set_facecolor('white')

x_formatter      = FormatStrFormatter('%3.2f')
# y_formatter      = FormatStrFormatter('%d')

ax.xaxis.set_minor_formatter(x_formatter)
ax.xaxis.set_major_formatter(x_formatter)
# ax.xaxis.set_minor_locator(MultipleLocator(50))
# ax.xaxis.set_major_locator(MultipleLocator(20))

# ax.yaxis.set_minor_formatter(y_formatter)
# ax.yaxis.set_major_formatter(y_formatter)
# ax.yaxis.set_minor_locator(MultipleLocator(50))
# ax.yaxis.set_major_locator(MultipleLocator(20))

plt.show()


# In[]:
