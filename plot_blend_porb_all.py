# -*- coding: utf-8 -*-


# In[]:

import os, sys
import glob
import numpy                as np

from collections            import OrderedDict

from astropy.io             import ascii as ascii_file
from astropy.table          import Table

import seaborn      as sns
# sns.set(rc={'image.cmap': 'spectral'})
# sns.set(rc={'image.cmap': 'Blues'})
# sns.set(rc={'image.cmap': 'PuBuGn'})
sns.set(rc={'image.cmap': 'Oranges'})

import matplotlib.pyplot    as plt
import matplotlib.cm        as cm
from matplotlib.ticker      import MultipleLocator, FormatStrFormatter
from matplotlib.ticker      import LogFormatter
from matplotlib             import colors

# my common functions - assume common folder is in path above this one
sys.path.append('../')
from common import utils
                     


# In[]:

simulation_headers=[]
simulation_headers.append('star_id')
simulation_headers.append('star_mass')
simulation_headers.append('star_luminosity')
simulation_headers.append('binary_id')
simulation_headers.append('binary_evol')
simulation_headers.append('binary_mass1')
simulation_headers.append('binary_mass2')
simulation_headers.append('star_mag_r')
simulation_headers.append('binary_mag_r')
simulation_headers.append('delta_mag')
simulation_headers.append('Period')
simulation_headers.append('a')
simulation_headers.append('transit_probability')
simulation_headers.append('binary_radius1')
simulation_headers.append('binary_radius2')
simulation_headers.append('binary_luminosity1')
simulation_headers.append('binary_luminosity2')
simulation_headers.append('actual_transit depth 1')
simulation_headers.append('actual_transit depth 2')
simulation_headers.append('measured_transit_depth_1')
simulation_headers.append('measured_transit_depth_2')


# In[]:


# Get all transit simulation files
# for an individual square

# run_folder = '/padata/beta/users/efarrell/data/github_plato/eclipse_output/run_5'
run_folder = '/padata/beta/users/efarrell/data/github_plato/blend_output/run_7'

run_subdirs =  utils.get_subdirs(run_folder)
squares     =  utils.filter_list(run_subdirs, remove='cluster_msgs')

# lots of messing to get sub-directories sorted
int_squares = map(int, squares)
int_squares.sort()
squares = map(str, int_squares)


print run_subdirs
print squares


# In[]:

# testing code
# only load some code
# squares = ['6', '16', '26']
squares = ['16']


# read all simulation files

x= np.array([], dtype=np.float64)
y= np.array([], dtype=np.float64)

for square in squares:
    print "current square is: " + str(square)

    current_folder = os.path.join(run_folder, square)
    pattern        = os.path.join(current_folder, "all_blends*")

    # load simulations from file
    simulations = utils.load_files(glob_pattern=pattern, headers=simulation_headers)


    for sim_filename, sim_data in simulations.iteritems():
        print "processing: " + sim_filename

        # ----------
        # Key point!
        # ----------
        # we're combining the output of
        # 20 different simulations
        x = np.concatenate((x, sim_data['actual_transit depth 1'].data), axis=0)
        y = np.concatenate((y, sim_data['Period'].data), axis=0)

        # # --------
        # # temp enda
        # # --------
        # # just take one simulation!
        # x = sim_data['eclipse_depth1'].data
        # y = sim_data['Porb'].data


# store this value.
# its used later to normalise the data
num_of_simulations = len(simulations)


# In[]:


plot_dir = './plots'
utils.rotate_folder(plot_dir)


# In[]:

# Plot a "log log" plot of eclipse depth v orbital period

fig, ax = plt.subplots()

period_label=r'$\log_{10}$ P [days]'
eclipse_label=r'$\log_{10}$(unblended eclipse depth)'

# ax.set_title('??? ', fontsize=16, fontweight='bold')
ax.set_xlabel(eclipse_label, fontsize=18 )
ax.set_ylabel(period_label,  fontsize=18 )


# mask invalid values
# x_log10 = np.log10(x)
# y_log10 = np.log10(y)
mask = (x > 0)
x_mask = x[mask]
y_mask = y[mask]
x_log10 = np.ma.log10(x_mask) 
y_log10 = np.ma.log10(y_mask) 
# x_log10 = np.ma.log10(x) 
# y_log10 = np.ma.log10(y) 



# this is a good one for 20 simulated runs
# _, _, _, hist_image = ax.hist2d(x_log10, y_log10, bins=[50,50] )
# _, _, _, hist_image = ax.hist2d(x_log10, y_log10, bins=[70,70], norm=colors.LogNorm(vmin=1, vmax=100))

# _, _, _, hist_image = ax.hist2d(x_log10, y_log10, bins=[70,70], normed=True, norm=colors.LogNorm())
# _, _, _, hist_image = ax.hist2d(x_log10, y_log10, bins=[70,70], normed=True, norm=colors.LogNorm(vmin=1, vmax=100))

_, _, _, hist_image = ax.hist2d(x_log10, y_log10, bins=[70,70], normed=True, norm=colors.Normalize())

# _, _, _, hist_image = ax.hist2d(x_log10, y_log10, bins=[100,100], norm=colors.LogNorm(vmin=1, vmax=100))
# _, _, _, hist_image = ax.hist2d(x_log10, y_log10, bins=[500,500], norm=colors.LogNorm())

# these 2 are for 1 run
# _, _, _, hist_image = ax.hist2d(x_log10, y_log10, bins=[50,50], norm=colors.LogNorm())
# _, _, _, hist_image = ax.hist2d(x_log10, y_log10, bins=[50,50])

# log_format = LogFormatter(10, labelOnlyBase=False)

# fig.colorbar(H[3], ax=ax)
# fig.colorbar(hist_image, ax=ax)
# fig.colorbar(hist_image, ax=ax, ticks=MultipleLocator(10))
cbar = fig.colorbar(hist_image, ax=ax)
# cbar = fig.colorbar(hist_image, ax=ax, format=log_format)

# cbar.set_ticks([0, 10, 100])
# cbar.set_ticklabels(['0', '1', '2'])



# ax.set_xlim([-10, 1]) # log


# ax.set_ylim([0, 2]) # years
# ax.set_ylim([0, 0.5]) # years



plt.show()

# In[]:


