# -*- coding: utf-8 -*-


# In[]:

import os, sys
import glob
import getopt


from collections        import OrderedDict
from astropy.io         import ascii as ascii_file
from astropy.table      import Table
from matplotlib.ticker  import MultipleLocator, FormatStrFormatter


import numpy                as np
import matplotlib.pyplot    as plt
import matplotlib.cm        as cm


# my 'common' functions -  
# should be in a folder above this one
sys.path.append('../')
from common import utils
                     


# In[]:

simulation_headers=['line_num', 'biseps_id', 
                    'lon', 'lat', 'r',
                    'mt1', 'mt2', 'reff1', 'reff2', 'lum1', 'lum2', 
                    'a', 'Porb', 'transit_prob', 
                    'eclipse_depth1', 'eclipse_depth2', 'inclin', 'met', 'evol']


# In[]:





# When debugging:
# Set 'run_folder' here
# run_folder = '/padata/beta/users/efarrell/data/plato/eclipse_output/run_8'


# When running from cluster job:
# Get 'run_folder' from command line 
opts, extra  = getopt.getopt(sys.argv[1:],'')
run_folder   = str(extra[0])


# double-check input parameters
if not os.path.exists(run_folder):
    print "Invalid parameter! Cannot find run_folder: " + run_folder
    print "Exiting program..."
    sys.exit()


# Get all eclipse simulation files
# from every sub-directory inside 'run_folder'

run_subdirs =  utils.get_subdirs(run_folder)
squares     =  utils.filter_list(run_subdirs, remove='cluster_msgs')

# some messing around just to 
# sort the sub-directories in order
int_squares = map(int, squares)
int_squares.sort()
squares = map(str, int_squares)


# helpful debug info
print run_subdirs
print squares


# define the output filenames
stats_filename = 'stats_summary'
hist_filename  = 'histogram_totals'


# In[]:

# create a HISTOGRAM of each simulation file,
# and get mean and standard deviation of each Histogram


# Histogram parameters
num_bins     = 50
start_range  = 0
end_range    = 0.5
x_ticks      = np.arange(start_range, end_range, 0.01)


# temp testing code
# squares = ['16']


for square in squares:
    print "square is: " + str(square)

    square_folder = os.path.join(run_folder, square)
    pattern       = os.path.join(square_folder, "simulated.*")

    # load simulated data files
    # into a dictionary object
    simulations   = utils.load_files(glob_pattern=pattern, headers=simulation_headers)

    # Summarise the transit depths 
    # from each run into a histogram
    # and write the histogram values
    # out to a CSV file...
    out_stats_file = os.path.join(square_folder, '.'.join([stats_filename, square, 'csv']))
    out_hist_file  = os.path.join(square_folder, '.'.join([hist_filename, square, 'csv']))

    print out_stats_file
    print out_hist_file


    # create a master array 
    # to hold binned data from 
    # each simulation run
    all_histograms = np.zeros(shape=(0, num_bins), dtype='int')


    # The 'all_histograms' array holds data like this:

    #                    bin1    bin2    bin3    bin4    etc...
    # sim file1:          181      53      17      8     ...
    # sim file2:          123      34      20      6     ...
    # sim file3:          170      45      15      8     ...
    #   ...               ...      ..      ..      .
    #   ...               ...      ..      ..      .

    #                       ^       ^       ^      ^ 
    #                       |       |       |      | 
    #                       |       |       |      | 
    # 'sim_mean' will hold --   -----   -----  ----- 
    # the mean of each 
    # column



    # Loop through each simulation file
    for sim_filename, sim_data in simulations.iteritems():
        print "processing: " + sim_filename

        # ----------
        # Key point!
        # ----------
        depth1 = sim_data['eclipse_depth1'].data
        depth2 = sim_data['eclipse_depth2'].data

        # combine the 2 depth arrays
        x = np.concatenate((depth1, depth2), axis=0)


        # create a histogram of the eclipse depths
        n, edges = np.histogram(x, 
                                bins=num_bins, 
                                range=[start_range, end_range])

        # add the new histogram 
        # to a master array
        # holding all histograms
        all_histograms = np.vstack([all_histograms, n])
        print "len all_histograms: " + str(len(all_histograms))


    # Save the results of each histogram to a file.
    # Each row has 'num_bins' columns,
    # with one column for each transit depth
    with open(out_hist_file,'w') as f:
            np.savetxt(f, all_histograms, fmt=['%d'] * num_bins, delimiter=",")


    # calculate the mean and standard deviation of each histogram COLUMN 
    # ('axis=0' tells numpy to take the mean of the columns not the rows)
    sim_mean    = np.mean(all_histograms, axis=0)
    sim_std     = np.std(all_histograms, axis=0)
    sim_std_log = np.ma.log10(sim_std)


    # write out the 'mean' and 'standard deviation' 
    # for each transit depth
    with open(out_stats_file,'w') as f:
        np.savetxt(f, zip(x_ticks, sim_mean, sim_std, sim_std_log), 
                   fmt=['%3.2f', '%d', '%5.3f', '%5.3f'], 
                   delimiter=",")



# In[]:

