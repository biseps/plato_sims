# -*- coding: utf-8 -*-

# In[]:

import os, sys, time, shutil
import getopt
import logging

import sqlite3 
import numpy as np

from astropy.io         import ascii as ascii_file 
from astropy.table      import vstack
from astropy.table      import hstack

# my common functions - 
# assume common folder is in path above this one
sys.path.append('../')
from common import utils
from common import constants as c


# In[]:

# constants
DATABASE_TABLE  = 'depths'

# # Code only for manual debugging
# grid_square     = '78'
# db_file         = '/padata/beta/users/efarrell/data/plato/jkt_output/run_1/' + grid_square + '/depths_' + grid_square + '.sqlite'
# mag_file        = '/padata/beta/users/efarrell/data/plato/pop_output/field1_5deg_26/run_1/pop_b_y/' + grid_square + '/mag.dat.1'
# extract_file    = '/padata/beta/users/efarrell/data/plato/pop_output/field1_5deg_26/run_1/pop_b_y/' + grid_square + '/extract.1'



# Get arguments from command line 
opts, extra  = getopt.getopt(sys.argv[1:],'')
mag_file     = str(extra[0])
extract_file = str(extra[1])
db_file      = str(extra[2])


# setup logging file to store debug messages
utils.set_logging_config(logging, log_level=logging.INFO, filebase=os.path.basename(db_file))
                

# write out debugging info
logging.info('%s', mag_file)
logging.info('%s', extract_file)
logging.info('%s', db_file)


# In[]:


def convert_porb_to_a(m1, m2, porb):
    """ Calculate semi-major axis

        Porb      is in days
        m1, m2    are in solar masses
        a         is in solar radii

    """

    # convert porb to years (divide by 365)
    n = 2.0 * np.pi / (porb/365.0)

    return (c.gn * (m1+m2) / n**2)**(1.0/3.0)





# In[]:

# Get stellar MAGNITUDES 
# from 'mag.dat' files
mag_headers = ['id', 'long', 'lat', 'u', 'g', 'r', 'i', 'z', 'd51', 'jj', 'jk', 'jh', 'kp']


# Get stellar PROPERTIES 
# from 'extract' files
extract_headers = ['line_num', 'biseps_id', 'mt1', 'reff1', 'teff1', 'lum1', 'mt2', 'reff2', 'teff2', 'lum2', 
        'Porb', 'birth', 'death', 'massTran', 'kw1', 'kw2', 'bkw', 'met', 'evol']
                                                           
# READ(10, *) num, id, &
            # s(1)%mt, s(1)%reff, s(1)%teff, s(1)%lum, &
            # s(2)%mt, s(2)%reff, s(2)%teff, s(2)%lum, &
            # b%porb, birth, death, mt, kw1, kw2, bkw, Z, BUFFER


mag_data      = ascii_file.read(mag_file,     delimiter=' ', names=mag_headers)
extract_data  = ascii_file.read(extract_file, delimiter=' ', names=extract_headers)

binary_table  = hstack([mag_data, extract_data])

# binary_table = hstack([mag_pop_b_y, extract_pop_b_y])




# In[]:

# Convert orbital Period of system 
# into semi-major axis
axes  = convert_porb_to_a(binary_table['mt1'].data, 
                          binary_table['mt2'].data, 
                          binary_table['Porb'].data) 


# calculate geometric probability of transit
radius1 = binary_table['reff1'].data
radius2 = binary_table['reff2'].data

transit_probs = (radius1 + radius2) / axes




# In[]:


# create a random inclination
# for every binary system we have
random_inclinations = np.random.randint(low=0, high=90, size=len(binary_table))


# load eclipse depths database into memory.
# database = sqlite3.connect(db_file)
database = utils.sqlite_to_memory(db_file)

logging.info('number of rows in binary table: %s', str(len(binary_table)))
row_counter = 0

with database:

    cursor = database.cursor()    


    # loop through every binary system 
    # in the 'binary_table' dataset
    for (row, axis, transit_prob, random_i) in zip(binary_table, axes, transit_probs, random_inclinations):

        row_counter += 1
        logging.debug("Row: %s", str(row_counter))
        logging.debug('Searching for: %s, %s, %s ', row['biseps_id'], random_i, row['met'])


        SQL_STRING  = 'select trans_depth1, trans_depth2, inclin '
        SQL_STRING += 'from {table} '
        SQL_STRING += 'where id     = {biseps_id} ' 
        SQL_STRING += 'and   inclin = {random_i} '
        SQL_STRING += 'and   z      = {metallicity} '
        SQL_STRING += 'limit 1 '


        # Search the database for the transit depth 
        # associated with this particular binary system, 
        # which has a certain metallicity,
        # for this particular inclination.
        query = SQL_STRING.format(table        = DATABASE_TABLE, 
                                  biseps_id    = row['biseps_id'], 
                                  random_i     = random_i,
                                  metallicity  = row['met'])

        results = cursor.execute(query)


        # old code not used:
        # for result in results.fetchall():

        # Note 
        # dont want to use 'result.fetchall()' because multiple
        # duplicate rows may be returned. The same star properties 
        # are used for many different long, lat coords

        # just one matching row will do
        result =  results.fetchone() 

        if result is None:
            # found nothing 
            # for this inclination!
            pass

        else:
            # get our values from the database
            depth1, depth2, inclin = result


            # create a LIST of all the values
            # we want to write out to a csv
            row_out = []
            row_out.append(str(row['line_num']))
            row_out.append(str(row['biseps_id']))
            row_out.append(str(row['long']))
            row_out.append(str(row['lat']))
            row_out.append(str(row['r']))
            row_out.append(str(row['mt1']))
            row_out.append(str(row['mt2']))
            row_out.append(str(row['reff1']))
            row_out.append(str(row['reff2']))
            row_out.append(str(row['lum1']))
            row_out.append(str(row['lum2']))
            row_out.append(str(axis))
            row_out.append(str(row['Porb']))
            row_out.append(str(transit_prob))
            row_out.append(str(depth1))
            row_out.append(str(depth2))
            row_out.append(str(inclin))
            row_out.append(str(row['met']))
            row_out.append(str(row['evol']))

            # write out the fields in csv format!
            # (i.e. joined by a comma)
            print ",".join(row_out)


logging.info("finished")
