# -*- coding: utf-8 -*-


# In[]:

import os, sys

from astropy.io         import ascii as ascii_file
from astropy.table      import Table
from matplotlib.ticker  import MultipleLocator, FormatStrFormatter
from matplotlib         import cm

import seaborn      as sns
import numpy        as np
import matplotlib
import matplotlib.pyplot as plt

# my common functions - assume common folder is in path above this one
sys.path.append('../')
from common   import utils


# In[]:

simulation_headers=['line_num', 'biseps_id', 
                    'lon', 'lat', 'r',
                    'mt1', 'mt2', 'reff1', 'reff2', 'lum1', 'lum2', 
                    'a', 'Porb', 'transit_prob', 
                    'transit_depth1', 'transit_depth2', 'inclin', 'met', 'evol']





# Get all transit simulation files
# for an individual square
base_folder = '/padata/beta/users/efarrell/data/plato'

# run_folder = base_folder + '/eclipse_output/run_1'
run_folder = base_folder + '/eclipse_output/run_8' # centre stripe, 1x1 deg

run_subdirs =  utils.get_subdirs(run_folder)
squares     =  utils.filter_list(run_subdirs, remove='cluster_msgs')

# lots of messing to get sub-directories sorted
int_squares = map(int, squares)
int_squares.sort()
squares = map(str, int_squares)


# create latitutes array 
# which is used in plot title
grid_height = 1
latitudes = np.arange(15, 65, grid_height)


print run_subdirs
print squares

stats_filename = 'stats_summary'
hist_filename  = 'histogram_totals'

# In[]:


# plot all periods and transit depths  
# on ONE plot

stats_data_headers=['x_ticks', 'sim_mean', 'sim_std', 'sim_std_log']

# for testing purposes only:
# squares = ['16', '17', '18']
# squares = ['6']


# scatter plot
fig = plt.figure(figsize=(20, 17))
ax = plt.subplot(111)

ax.set_title('Average Eclipse Depth (as a function of latitude)', fontsize=16, fontweight='bold')
ax.set_xlabel('Eclipse Depth', fontsize=18, fontweight='bold', labelpad=20)
ax.set_ylabel('Number Binary Systems', fontsize=18, fontweight='bold', labelpad=20)

colors = cm.Dark2(np.linspace(0, 0.75, len(squares)))
sigma_color = 'yellow'


# got through each grid square
# and plot the average eclipse depth
for (i, square) in enumerate(squares):

    if i % 5 != 0:
        # dont plot every single square
        # skip some of them
        pass
    else:
        print 'plotting: ' +  square

        # read the statistics summary file
        current_folder = os.path.join(run_folder, square)
        stat_filename  = '.'.join([stats_filename, square, 'csv'])
        stat_file      = os.path.join(current_folder, stat_filename)

        print "current_folder is: " + current_folder
        print "stat file: " + stat_file

        label         = str(latitudes[i]) + r'$^\circ$'
        current_color = colors[i]


        # read the csv file
        stats_data = ascii_file.read(stat_file, names=stats_data_headers)

        x_mean = stats_data['x_ticks'].data[1:]
        y_mean = stats_data['sim_mean'].data[1:]
        std    = stats_data['sim_std'].data[1:]

        ax.plot(x_mean, y_mean, 
                color=current_color, linestyle='-', linewidth=1, marker='o',
                markerfacecolor=current_color, markersize=5, 
                label=label)

            
        lower_bound = y_mean - std
        upper_bound = y_mean + std

        # ax.fill_between(x_mean, lower_bound, upper_bound, facecolor=sigma_color, alpha=0.2, label='1 sigma range')


# sigma_box = plt.Rectangle((0, 0), 1, 1, fc=sigma_color)

handles, labels = ax.get_legend_handles_labels()
# handles.append(sigma_box)
# labels.append('1 sigma range')
# ax.legend(handles, labels)

legend = ax.legend(handles, labels, title='Latitude', loc='center right', numpoints=3, frameon=True, framealpha=1, fancybox=True, fontsize='x-large', shadow=True)
frame = legend.get_frame()
frame.set_facecolor('white')

legend.get_title().set_fontsize('x-large')

# ax.set_xlim([-0.1,0.6])
ax.set_xlim([0,0.5])
# ax.set_ylim([0, 150])

yFormatter      = FormatStrFormatter('%d')
xFormatter      = FormatStrFormatter('%3.2f')
# ax.yaxis.set_minor_formatter( yFormatter )
# ax.xaxis.set_minor_formatter( xFormatter )



# yminorLocator   = MultipleLocator(50)
# ax.yaxis.set_minor_locator( yminorLocator )
ax.set_yscale("log")

# ymajorLocator   = MultipleLocator(10)
# ax.yaxis.set_major_locator( ymajorLocator )

# xminorLocator   = MultipleLocator(0.01)
ax.xaxis.set_major_locator( MultipleLocator(0.02) )
# ax.xaxis.set_minor_locator( MultipleLocator(0.01) )


# for label in ax.get_xticklabels(minor=True):
    # label.set_rotation(45)

for label in ax.get_xticklabels():
    label.set_rotation(45)

# xlabels = ax.get_xticklabels()
# ax.set_xticklabels(xlabels, rotation=40)

# ax.minorticks_on() 
# ax.grid(True, which='minor')
ax.grid(True, which='major')
# ax.grid(True, which='both')


plt.show()





# In[]:



