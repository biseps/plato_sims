# -*- coding: utf-8 -*-


# get initial masses from Biseps sqlite files
# using a list of line numbers that were output
# by population.f90 (or would work with ranloc.f90 too)


# In[]:

import os, sys, time, shutil
import getopt
import logging


import sqlite3 
import numpy as np
from astropy.io import ascii as ascii_file 


# my stuff
sys.path.append('../')
from common import utils
from common import constants as c


# In[]:

# constants
CONST_DB_TABLE  = 'web_pop'

# Code only for manual debugging
base_dir       =  '/padata/beta/users/efarrell/data/'

# # sub-solar metalicity
# db_file         = os.path.join(base_dir, 'biseps2_runs/07_run/subSol', 'subsol.sqlite')
# ranpick_file    = os.path.join(base_dir, 'plato/pop_output/field1_5deg_26/run_1/pop_b_o/78', 'ranPick.dat.1')

# solar metalicity
db_file         = os.path.join(base_dir, 'biseps2_runs/07_run/sol', 'sol.sqlite')
ranpick_file    = os.path.join(base_dir, 'plato/pop_output/field1_5deg_26/run_1/pop_b_y/78', 'ranPick.dat.1')



# # Get command line arguments
# opts, extra = getopt.getopt(sys.argv[1:],'')
# mag_file     = str(extra[0])
# extract_file = str(extra[1])
# db_file      = str(extra[2])


# setup file for debug messages
utils.set_logging_config(logging, log_level=logging.INFO, filebase=os.path.basename(db_file))
                

# debugging
logging.info('%s', ranpick_file)



# In[]:


# Get line number 
# from 'ranpick' file
headers = ['line_num']
                                                           

# ran_pick = ascii_file.read(ranpick_file,   delimiter=' ', names=headers)
ran_pick = ascii_file.read(ranpick_file, format='fixed_width_no_header', names=headers)


# In[]:



# load database 
database = sqlite3.connect(db_file)
# database = utils.sqlite_to_memory(db_file)

logging.info('length ran pick: %s', str(len(ran_pick)))
row_counter = 0


# In[]:

with database:

    cur = database.cursor()    


    for row in ran_pick:


        row_counter += 1
        logging.debug("Row: %s", str(row_counter))
        logging.debug('Searching for: %s', row['line_num'])


        SQL_STRING  = 'select num, mt1, mt2, Porb  '
        SQL_STRING += 'from {table} '
        SQL_STRING += 'where rowid = {biseps_line_num} ' 

        query = SQL_STRING.format(table             = CONST_DB_TABLE, 
                                  biseps_line_num   = row['line_num'])

        results = cur.execute(query)


        # just first matching row will do
        result =  results.fetchone() 

        if result is None:
            # found nothing 
            # for this inclination!
            logging.info("Not found: %s", row['line_num'])
            # pass

        else:
            # get our values from the database
            num, mt1, mt2, Porb = result


            # Write everything out to a file!
            rowout = []
            rowout.append(str(row['line_num']))
            rowout.append(str(num))
            rowout.append(str(mt1))
            rowout.append(str(mt2))
            rowout.append(str(Porb))

            # create csv output!
            print ",".join(rowout)


logging.info("finished")

# In[]:

